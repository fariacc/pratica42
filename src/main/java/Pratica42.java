import java.lang.Math;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Circulo;

public class Pratica42
{
    public static void main(String[] args)
    {
      Elipse elip = new Elipse(2.5, 1);
      System.out.println(elip.getArea());
      System.out.println(elip.getPerimetro());


      Circulo circ = new Circulo(5);
      System.out.println(circ.getArea());
      System.out.println(circ.getPerimetro());
    }
}