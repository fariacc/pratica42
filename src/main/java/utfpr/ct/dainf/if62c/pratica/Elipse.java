package utfpr.ct.dainf.if62c.pratica;

public class Elipse implements FiguraComEixos 
{
	public double r;
  	public double s;
    	
  public Elipse(double semi_eixo_r, double semi_eixo_s) 
  {
  	r = 2 * semi_eixo_r;
  	s = 2 * semi_eixo_s;
  }
  
  @Override
  public String getNome() 
  {
    return this.getClass().getSimpleName();
  }
  
  @Override
  public double getPerimetro() 
  {
    return Math.PI * ( 3 * ( r + s ) - Math.sqrt( ( 3*r + s ) * ( r + 3*s ) ) );
  }
  
  @Override
  public double getArea() 
  {
    return Math.PI * r * s;
  }
  
  @Override
  public double getEixoMenor() 
  {
    return r < s ? r : s;
  }
  
  @Override
  public double getEixoMaior() 
  {
    return r > s ? r : s;
  }
}